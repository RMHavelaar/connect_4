
(function(document) {
//   Lets start the game and establish some variables and functions
    let start = function() {
        let finished = false;
        changePlayer();
    },  
        // This is set up for the Reset button that was initially done with JS but is now in CSS.  
        // I left it in as to not break the code.  It can be removed if I revisit this.
    newGame = function(message) {
        if (confirm(message)) {
            start();
            slots(emptySlot);
        }
    },  

    element = function(id) {
        return document.getElementById(id);
    },

    value = function(el) {
        return element(el).innerHTML;
    },  

    cell = function(i,j) {
        return element("c-"+ i +"-"+j);
    }, 

    slots = function(action) {
        for (let t = 1;t < 7;t++) {
            for (let counter2 = 1;counter2 < 8;counter2++) {
                action(t,counter2);
            }
        }
    },    

    sameColor = function(i,j) {
        return testClass(i,j,players[current]);
    }, 

    changePlayer = function() {
        element("c").innerHTML = players[current = (current + 1) % 2];
    }, 
// Winning conditions
    horizontalWin = function(i,j) {
        for(min= j- 1;min > 0;min--)
            if(!sameColor(i,min))
                break;					
        for(max=j + 1;max < 8;max++)
            if(!sameColor(i,max))
                break;
        return max-min>4;
    },

    verticalWin = function(i,j) {
        for(max=i + 1;max < 7;max++)
            if(!sameColor(max,j))
                break;
        return max - i > 3;
    },    

    diagonalLeftToRightWin = function(i,j) {
        for(min=i - 1,t= j - 1;min > 0;min--,t--)
            if(t < 1 || !sameColor(min,t))
                break;
        for(max=i + 1,t=j + 1;max < 7;max++,t++)
            if(t > 7 || !sameColor(max,t))
                break;
        return max-min>4;
    },
  
    diagonalRightToLeftWin = function(i,j) {
        for(min=i - 1,t=j + 1;min > 0;min--,t++)
            if(t > 7 || !sameColor(min,t))
                break;
        for(max=i + 1,t=j - 1;max < 7;max++,t--)
            if(t < 1 || !sameColor(max,t))
                break;
        return max-min>4;
    },   

   colorSlot = function(i,j,color) {
        cell(i,j).className = color;
    }, 

    emptySlot = function(i,j) {
       colorSlot(i,j,'');
    },
 
    testClass = function(i,j,value) {
        return cell(i,j).className == value;
    },
// Now lets move those disk around until we win or tie.
    addCellAction = function(i,j) {
        cell(i,j).onclick = function(j) {
            return function() {
                if(!finished) {
                    for(t = 6;t > 0;t--) {
                        if(testClass(t,j,'')) {
                           colorSlot(t,j,players[current]);
                            if(horizontalWin(t,j) || verticalWin(t,j) || diagonalLeftToRightWin(t,j) || diagonalRightToLeftWin(t,j)){
                                finished = true;
                                newGame(WinMessage.replace("%s",players[current]));
                            } else {
                                changePlayer();
                            }
                            break;
                        }
                    }
                }
            }
        }(j); 
    },

    // Alright we've got a winner,  Lets let them know!
    players = [value("redDisc"),value("blackDisc")],         
    current = 0,
    newGameMessage = value("newGame"),
    WinMessage = value("winner"),
    finished;
    start();
    slots(addCellAction);
//          ---DEAD BUTTON---
//     element("retry").onclick = function(){
//         newGame(newGameMessage)
//         const element =  document.querySelector('cell')
// element.classList.add('animated', 'bounceOutLeft')
    
})(document);

//      ---Modal code sourced from W3schools---
// https://www.w3schools.com/howto/howto_css_modals.asp
let modal = document.getElementById("instructionModal")
let btn = document.getElementById("instructionsButton")
let span = document.getElementsByClassName("close")[0];
btn.onclick = function() {
    modal.style.display = "block";
  }

span.onclick = function() {
    modal.style.display = "none";
  }

window.onclick = function(event) {
    if (event.target == modal) {
      modal.style.display = "none";
    }
  }
