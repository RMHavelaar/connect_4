                    ---Connect 4---
                       --v.2--
                  --Robert havelaar--



--GitLab Pages Link https://rmhavelaar.gitlab.io/connect_4 


Questions:
Answered all questions.

Extra:  How can I animate the balls falling in?


<!-- 
I would like to start off with the frame work of this code is not mine.  I appreciated Randy's demo and have completed about 90% of that code.  If there is an issue with this I can turn in his framework as well.

Not a single line was copy and pasted (expect for the Modal code). I typed everything myself.  

That being said I treat this project very similarly to what Randy did for us.  I watched many videos and found some code that would help me implement a table into my JavaScript.  You will see a lot of similar code just like what randy did for us in his demo.  I chose to stick with the path of a table because I understood what randy was teaching and wanted to learn another method, and I've seen tables used before and had to jump on the chance to use one.  I had so much fun and spent so much time tweaking and making all of this my own.  I am very proud of it and the knowledge I gained about using tables. The link for my resource is https://gist.github.com/jslegers/6054902. 
-->


Development Plan:
1. Create a 7x6 game board with 2 colors of discs.
  1. Square game board with a grid of 7x6 equally sized circles.
    1. Using a table to define each row/col.
  2. Create 2 graphic disc's
    1. Create a playerColor array and fill the array with p1/p2
      2. Call the Player color variable and input a color string.

2. Allow 2 players to play verse each other.
  1. Assign 2 players
    1. For each turn, activate a player and deactivate the other.
      2. As turns change so should the colors set up in 1,2
  2. Enforce game rules for placing disc's
    1. Disc's must be placed within the table to drop.
      2. Only one disc per "slot"
        3. Each player only gets 1 disc per turn.
  3. Allow winning condition.
    1. Player "wins" the game by lining up 4 of their disc's in a row.
      1. 4 in a row Horizontal, Vertical,or Diagonal wins.

3. End game, Start over.
  1. Once winning condition is met "alert" the winners name.
    1. alert("Player X Wins!")
  2. Provide an option to play again.
    1. Add a button that refreshes just the table, to allow a new match to start.






    